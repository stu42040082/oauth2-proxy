


helm repo add bitnami https://charts.bitnami.com/bitnami
kubectl apply -f namespace.yaml
helm upgrade -install oauth2-proxy-rel --set auth.password=redisadmin -f local.values.yaml bitnami/oauth2-proxy -n oauth2-proxy



# References

1. https://github.com/bitnami/charts/tree/master/bitnami/oauth2-proxy/#installing-the-chart
2. https://medium.com/@senthilrch/api-access-control-using-istio-ingress-gateway-44be659a087e
3. https://medium.com/@senthilrch/api-authentication-using-istio-ingress-gateway-oauth2-proxy-and-keycloak-a980c996c259
4. https://medium.com/@senthilrch/api-authentication-using-istio-ingress-gateway-oauth2-proxy-and-keycloak-part-2-of-2-dbb3fb9cd0d0
5. https://oauth2-proxy.github.io/oauth2-proxy/docs/configuration/oauth_provider#keycloak-oidc-auth-provider

 